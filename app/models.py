import pytz
from . import db
from sqlalchemy.sql import func
from flask_login import UserMixin

class Log(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  log_type = db.Column(db.String(100), default="default")
  data = db.Column(db.String(1000))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())
  user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

# UserMixin is used to implement login
class User(db.Model, UserMixin):
  id = db.Column(db.Integer, primary_key=True)
  email = db.Column(db.String(150), unique=True)
  password = db.Column(db.String(300))
  first_name = db.Column(db.String(150))
  last_name = db.Column(db.String(150))
  role = db.Column(db.String(150), default="user")
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())
  logs = db.relationship('Log')

class Video(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  video_name = db.Column(db.String(150), unique=True)
  title_id = db.Column(db.Integer, db.ForeignKey('title.id'))
  signer = db.Column(db.String(150))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())
  title = db.relationship('Title')

class Frame(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  folder = db.Column(db.String(150))
  image_name = db.Column(db.String(150), unique=True)
  label_id = db.Column(db.Integer, db.ForeignKey('label.id'))
  word_id = db.Column(db.Integer, db.ForeignKey('word.id'))
  video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())
  label = db.relationship('Label')
  word = db.relationship('Word')
  video = db.relationship('Video')
  


class Label(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  label = db.Column(db.String(50))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())

class Word(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  word = db.Column(db.String(50))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())

class Title(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  gloss = db.Column(db.String(250))
  word = db.Column(db.String(250))
  text = db.Column(db.String(250))
  created_at = db.Column(db.DateTime(timezone=True), default=func.now())
  updated_at = db.Column(db.DateTime(timezone=True), default=func.now(), onupdate=db.func.now())

def __repr__(self):
    return f'<User: {self.email}>'