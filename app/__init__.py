import os
import pytz

from flask import Flask
from dotenv import load_dotenv
from sqlalchemy.orm import Session
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

load_dotenv()
db_server = os.getenv('db_server')
db_port = os.getenv('db_port')
db_user = os.getenv('db_user')
db_password = os.getenv('db_password')
db_name = os.getenv('db_name')

db = SQLAlchemy()
timezone = pytz.timezone('Asia/Jakarta')

def create_app():
  # initialitation
  app = Flask(__name__)
  app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024 
  app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
  # app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
  app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+mysqlconnector://{db_user}:{db_password}@{db_server}:{db_port}/{db_name}'

  # create db
  from .models import User, Log
  db.init_app(app)
  with app.app_context():
    db.create_all()
    print("Created Database")

  # import blueprints
  from .routes.contents import contents
  from .routes.auth import auth
  from .routes.logs import logs
  from .routes.users import users
  from .routes.videos import videos
  from .routes.frames import frames
  from .routes.crud import cruds
  from .routes.labels import labels
  from .routes.words import words
  from .routes.titles import titles

  # register blueprint
  app.register_blueprint(contents, url_prefix='/')
  app.register_blueprint(auth, url_prefix='/')
  app.register_blueprint(logs, url_prefix='/')
  app.register_blueprint(users, url_prefix='/')
  app.register_blueprint(videos, url_prefix='/')
  app.register_blueprint(frames, url_prefix='/')
  app.register_blueprint(cruds, url_prefix='/')
  app.register_blueprint(labels, url_prefix='/')
  app.register_blueprint(words, url_prefix='/')
  app.register_blueprint(titles, url_prefix='/')

  login_manager = LoginManager()
  login_manager.login_view = 'auth.login'
  login_manager.init_app(app)

  @login_manager.user_loader
  def load_user(id):
    return User.query.get(int(id))

  return app