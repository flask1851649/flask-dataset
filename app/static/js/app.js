function deleteNote(noteId){
  fetch("/delete-log", {
    method: "POST",
    body: JSON.stringify({noteId: noteId}),
  }).then((_res)=>{
    window.location.href = "/log-list";
  });
}

$(document).ready(function () {
  // show the alert
  setTimeout(function () {
    $(".alert").alert("close");
  }, 2000);

  // show the modal
  $("a.btn-success").click(function () {
    var myModal = new bootstrap.Modal(document.getElementById("myModal"),{ keyboard: false });

    var myVar = $(this).data("my-var");
    $("#video").attr("src", "/videos_datasets/" + myVar);
    $("#video").parent()[0].load();

    var myTitle = $(this).data("my-title");
    myTitle = myTitle + " (" + myVar + ")";
    $("#modal-title").html(myTitle);
    myModal.show();

  });

  // show the modal
  $("img.img-thumbnail").click(function () {
    var myModal = new bootstrap.Modal(document.getElementById("myModal"),{ keyboard: false });
    
    var myVar = $(this).data("my-var");
    $("#frame").attr("src", myVar);
    // $("#frame").parent()[0].load();
    myModal.show();
  });
});