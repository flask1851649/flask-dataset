from app import db
from app.models import Log
from datetime import datetime
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for

# create Blueprint
logs = Blueprint('logs', __name__)

@logs.route('/log-add', methods=['GET', 'POST'])
@login_required
def log_add():
  log_type = request.form.get('log_type')
  data = request.form.get('data')
  if request.method == 'POST':
    if len(data) < 1:
      # send flash message
      flash('Log is too short', category='error')
      return render_template('logs/add.html', user=current_user)
    else:
      # add a new data
      new_log = Log()
      new_log.log_type=log_type
      new_log.data=data
      new_log.user_id = current_user.id
      new_log.created_at = datetime.now().replace(microsecond=0)
      new_log.updated_at = datetime.now().replace(microsecond=0)
      db.session.add(new_log)
      db.session.commit()
      # send flash message
      flash('Log added!', category='success')
    return redirect(url_for('logs.logs_list'))
  else:
    return render_template('logs/add.html', user=current_user)
  
@logs.route('/log-list', methods=['GET'])
@login_required
def logs_list():
  user = current_user
  logs = Log.query.filter_by(user_id=user.id).order_by(Log.updated_at.desc()).all()
  return render_template('logs/list.html', logs=logs ,user=user)

@logs.route('/log-edit/<logId>', methods=['GET','POST'])
@login_required
def log_edit(logId):
  if request.method == 'GET':
    data = Log.query.get(logId)
    return render_template('logs/edit.html', data=data, user=current_user)
  else:
    # edit data in database
    logId = request.form.get('id')
    log = Log.query.get(logId)
    log_type = request.form.get('log_type')
    data = request.form.get('data')
    log.log_type =  log_type
    log.data = data
    log.updated_at = datetime.now().replace(microsecond=0)
    db.session.commit()
    # send flash message
    flash('Log updated!.', category='success')
    return redirect(url_for('logs.logs_list'))

@logs.route('/log-del/<logId>', methods=['GET','POST'])
@login_required
def log_del(logId):
  log = Log.query.get(logId)
  if log: 
    if request.method == 'GET':
      log = Log.query.get(logId)
      return render_template('logs/del.html', log=log, user=current_user)
    elif  request.method == 'POST':
      if log.user_id == current_user.id:
        # delete data from database
        db.session.delete(log)
        db.session.commit()
        # send flash message
        flash('Log deleted!', category='success')
  return redirect(url_for('logs.logs_list'))


# @logs.route('/delete-log', methods=['POST'])
# def delete_log():
#   log = json.loads(request.data)
#   print (log)
#   logId = log['logId']
#   log = log.query.get(logId)
#   if log:
#     if log.user_id == current_user.id:
#       db.session.delete(log)
#       db.session.commit()
#   return jsonify({})


