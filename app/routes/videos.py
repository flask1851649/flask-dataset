import os
import cv2
import uuid
import shutil


from app import db
from app.models import Video, Frame, Title
from datetime import datetime
from werkzeug.utils import secure_filename
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for, send_from_directory

VIDEO_FOLDER = 'app/datasets/videos/'
FRAME_FOLDER = 'app/datasets/frames/'
ALLOWED_EXTENSIONS = {'mp4'}

# create Blueprint
videos = Blueprint('videos', __name__)

def allowed_file(filename):
  return '.' in filename and \
          filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def get_file_name(filename):
  return os.path.splitext(filename)[0]

def save_to_frame(filename, video_id):
  cap = cv2.VideoCapture(os.path.join(VIDEO_FOLDER, filename))
  n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
  path = os.path.join(FRAME_FOLDER, get_file_name(filename))
  # create frame folder
  os.mkdir(path)  
  for frame in range(n_frames):
    ret, image = cap.read()
    if ret == False:
        break
    # generate filename
    name = os.path.splitext(filename)[0] + '-' + '{0:0>3}'.format(frame) + '.jpg'
    image_name = os.path.join(path,name)
    cv2.imwrite(image_name,image)
    # add data to database
    new_frame = Frame(folder=get_file_name(filename), image_name=name, video_id=video_id)
    db.session.add(new_frame)
    db.session.commit()
  cap.release()

# Custom static data
@videos.route('/videos_datasets/<path:filename>')
def videos_datasets(filename):
  return send_from_directory('datasets/videos/', filename)

@videos.route('/video-add', methods=['GET', 'POST'])
@login_required
def video_add():
  if current_user.role == 'admin':
    if request.method == 'POST':
      if 'file' not in request.files:
        # send flash message
        flash('No file part', category='error')
        return redirect(request.url)
      
      # get file  
      file = request.files['file'] 
      if file.filename == '':
        # send flash message
        flash('No selected file', category='error')
        return redirect(request.url)
      elif file and allowed_file(file.filename):
        # save file
        filename = secure_filename(file.filename)
        file.save(os.path.join(VIDEO_FOLDER, filename))
        # input data to database
        signer = request.form.get('signer')
        title_id = request.form.get('title_id')
        new_video = Video(video_name=filename, signer=signer, title_id=title_id)
        db.session.add(new_video)
        db.session.commit()
        # save frame to folder 
        save_to_frame(filename,new_video.id)
        # send flash message
        flash('Video added!', category='success')
        return redirect(url_for('videos.videos_list'))
      else:
        # send flash message
        flash('Not allowed file', category='error')
        return redirect(request.url)
    else:
      titles = Title.query.order_by(Title.text.asc()).all()
      return render_template('videos/add.html', user=current_user, titles=titles)

@videos.route('/video-del/<videoId>', methods=['GET','POST'])
@login_required
def video_del(videoId):
  video = Video.query.get(videoId)
  if video: 
    if request.method == 'GET':
      return render_template('videos/del.html', video=video, user=current_user)
    elif  request.method == 'POST':
      # delete files
      file_path = os.path.join(VIDEO_FOLDER, video.video_name)
      if os.path.isfile(file_path):
        # delete video
        os.remove(file_path)
        # delete frame folder
        frame_folder = os.path.join(FRAME_FOLDER, get_file_name(video.video_name))
        if os.path.exists(frame_folder):
          # delete folder frame
          shutil.rmtree(frame_folder)
      # delete data from database
      frames = Frame.__table__.delete().where(Frame.video_id==video.id)
      db.session.execute(frames)
      db.session.delete(video)
      db.session.commit()
      # send flash message
      flash('Video deleted!', category='success')
  return redirect(url_for('videos.videos_list'))

@videos.route('/video-edit/<videoId>', methods=['GET','POST'])
@login_required
def video_edit(videoId):
  video = Video.query.get(videoId)
  if video: 
    if request.method == 'GET':
      titles = Title.query.order_by(Title.text.asc()).all()
      return render_template('videos/edit.html', video=video, user=current_user, titles=titles)
    elif  request.method == 'POST':
      # edit data in database
      signer = request.form.get('signer')
      title_id = request.form.get('title_id')
      video.signer = signer
      video.title_id = title_id
      # commit db
      db.session.commit()
      # send flash message
      flash('Video edited!', category='success')
  return redirect(url_for('videos.videos_list'))

@videos.route('/video-list', methods=['GET','POST'])
def videos_list():
  signer = request.form.get('signer')
  title_id = request.form.get('title_id', type=int)
  titles = Title.query.order_by(Title.text.asc()).all()
  videos = Video.query.order_by(Video.video_name.asc())
  
  if request.method == 'GET':
    signer = 'signer-1'
  if(signer):
    videos = videos.filter(Video.signer==signer)
  if(title_id):
    videos = videos.filter(Video.title_id==title_id)
  
  videos.all()  
  return render_template('videos/list.html', videos=videos ,user=current_user, titles=titles, title_id=title_id, signer=signer)