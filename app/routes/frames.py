import os

from app import db
from app.models import Frame, Label, Word
from datetime import datetime
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for,send_from_directory

# create Blueprint
frames = Blueprint('frames', __name__)

FRAME_FOLDER = 'app/datasets/frames/'

# Custom static data
@frames.route('/frames_datasets/<foldername>/<path:filename>')
def frames_datasets(foldername, filename):
  return send_from_directory('datasets/frames/'+foldername+'/', filename)

@frames.route('/frame-edit/<video_id>', methods=['GET','POST'])
@login_required
def frame_edit(video_id):
  if request.method == 'GET':
    # get label data
    labels = Label.query.order_by(Label.label.asc()).all()
    words = Word.query.order_by(Word.word.asc()).all()      
    #get frame data
    frames = Frame.query.filter(Frame.video_id==video_id).all()
    return render_template('frames/edit.html', frames=frames ,labels=labels, words=words, user=current_user)
  else:
    labels = request.form.getlist('label')
    labels_id = request.form.getlist('label_id')
    words = request.form.getlist('word')
    words_id = request.form.getlist('word_id')
    ids = request.form.getlist('id')
    index = 0
    for label_id in labels_id:
      frame = Frame.query.get(ids[index])
      # frame.label = label
      frame.label_id = label_id
      # frame.word = words[index]
      frame.word_id = words_id[index]
      frame.updated_at = datetime.now().replace(microsecond=0)
      db.session.commit()
      index += 1
  return redirect(url_for('videos.videos_list'))
  