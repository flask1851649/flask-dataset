from app import db,timezone
from app.models import Label
from datetime import datetime, timedelta
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for

# create Blueprint
labels = Blueprint('labels', __name__)

@labels.route('/label-add', methods=['GET', 'POST'])
@login_required
def label_add():
  id = request.form.get('id')
  label = request.form.get('label')
  created_at = request.form.get('created_at')
  updated_at = request.form.get('updated_at')
  if request.method == 'POST':
    if len(label) < 1:
      flash('label is too short', category='error')
      return render_template('label/add.html', user=current_user)
    else:
      flash('label added!', category='success')
      new_label = Label()
      # new_label.id=id
      new_label.label=label
      new_label.created_at=datetime.now().replace(microsecond=0)
      new_label.updated_at=datetime.now().replace(microsecond=0)
      db.session.add(new_label)
      db.session.commit()
    return redirect(url_for('labels.labels_list'))
  else:
    return render_template('labels/add.html', user=current_user)

@labels.route('/label-list', methods=['GET'])
@login_required
def labels_list():
  user = current_user
  labels = Label.query.order_by(Label.label.asc()).all()
  return render_template('labels/list.html', labels=labels ,user=user)

@labels.route('/label-edit/<labelId>', methods=['GET','POST'])
@login_required
def label_edit(labelId):
  if request.method == 'GET':
    data = Label.query.get(labelId)
    return render_template('labels/edit.html', data=data, user=current_user)
  else:
    data = Label.query.get(labelId)
    # id = request.form.get('id')
    label = request.form.get('label')
    created_at = request.form.get('created_at')
    updated_at = request.form.get('updated_at')
    # data.id =  id
    data.label =  label
    # data.created_at =  created_at
    data.updated_at =  datetime.now().replace(microsecond=0)
    db.session.commit()
    flash('Label updated!.', category='success')
    return redirect(url_for('labels.labels_list'))

@labels.route('/label-del/<labelId>', methods=['GET','POST'])
@login_required
def label_del(labelId):
  label = Label.query.get(labelId)
  if label: 
    if request.method == 'GET':
      label = Label.query.get(labelId)
      return render_template('labels/del.html', data=label, user=current_user)
    elif  request.method == 'POST':
      db.session.delete(label)
      db.session.commit()
      flash('Label deleted!', category='success')
  return redirect(url_for('labels.labels_list'))
