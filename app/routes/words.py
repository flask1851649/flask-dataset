from app import db,timezone
from app.models import Word
from datetime import datetime, timedelta
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for

# create Blueprint
words = Blueprint('words', __name__)

@words.route('/word-add', methods=['GET', 'POST'])
@login_required
def word_add():
  id = request.form.get('id')
  word = request.form.get('word')
  created_at = request.form.get('created_at')
  updated_at = request.form.get('updated_at')
  if request.method == 'POST':
    if len(word) < 1:
      flash('word is too short', category='error')
      return render_template('words/add.html', user=current_user)
    else:
      flash('word added!', category='success')
      new_word = Word()
      # new_word.id=id
      new_word.word=word
      new_word.created_at=datetime.now().replace(microsecond=0)
      new_word.updated_at=datetime.now().replace(microsecond=0)
      db.session.add(new_word)
      db.session.commit()
    return redirect(url_for('words.words_list'))
  else:
    return render_template('words/add.html', user=current_user)

@words.route('/word-list', methods=['GET'])
@login_required
def words_list():
  user = current_user
  words = Word.query.order_by(Word.word.asc()).all()
  return render_template('words/list.html', words=words ,user=user)

@words.route('/word-edit/<wordId>', methods=['GET','POST'])
@login_required
def word_edit(wordId):
  if request.method == 'GET':
    data = Word.query.get(wordId)
    return render_template('words/edit.html', data=data, user=current_user)
  else:
    data = Word.query.get(wordId)
    id = request.form.get('id')
    word = request.form.get('word')
    created_at = request.form.get('created_at')
    updated_at = request.form.get('updated_at')
    data.id =  id
    data.word =  word
    # data.created_at =  created_at
    data.updated_at =  datetime.now().replace(microsecond=0)
    db.session.commit()
    flash('Word updated!.', category='success')
    return redirect(url_for('words.words_list'))

@words.route('/word-del/<wordId>', methods=['GET','POST'])
@login_required
def word_del(wordId):
  word = Word.query.get(wordId)
  if word: 
    if request.method == 'GET':
      word = Word.query.get(wordId)
      return render_template('words/del.html', data=word, user=current_user)
    elif  request.method == 'POST':
      db.session.delete(word)
      db.session.commit()
      flash('Word deleted!', category='success')
  return redirect(url_for('words.words_list'))
