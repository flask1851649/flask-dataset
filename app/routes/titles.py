from app import db
from app.models import Title
from datetime import datetime
from flask_login import login_required, current_user
from flask import Blueprint,render_template, request, flash, jsonify, redirect, url_for

# create Blueprint
titles = Blueprint('titles', __name__)

@titles.route('/title-add', methods=['GET', 'POST'])
@login_required
def title_add():
  # id = request.form.get('id')
  gloss = request.form.get('gloss')
  word = request.form.get('word')
  text = request.form.get('text')
  # created_at = request.form.get('created_at')
  # updated_at = request.form.get('updated_at')
  if request.method == 'POST':
    if len(gloss) < 1:
      flash('Gloss is too short', category='error')
      return render_template('titles/add.html', user=current_user)
    elif len(word) < 1:
      flash('Word is too short', category='error')
      return render_template('titles/add.html', user=current_user)
    elif len(text) < 1:
      flash('Text is too short', category='error')
      return render_template('titles/add.html', user=current_user)
    else:
      flash('title added!', category='success')
      new_title = Title()
      # new_title.id=id
      new_title.gloss=gloss
      new_title.word=word
      new_title.text=text
      # new_title.created_at=created_at
      # new_title.updated_at=updated_at
      db.session.add(new_title)
      db.session.commit()
    return redirect(url_for('titles.titles_list'))
  else:
    return render_template('titles/add.html', user=current_user)

@titles.route('/title-list', methods=['GET'])
@login_required
def titles_list():
  user = current_user
  titles = Title.query.order_by(Title.text.asc()).all()
  return render_template('titles/list.html', titles=titles ,user=user)

@titles.route('/title-edit/<titleId>', methods=['GET','POST'])
@login_required
def title_edit(titleId):
  if request.method == 'GET':
    data = Title.query.get(titleId)
    return render_template('titles/edit.html', data=data, user=current_user)
  else:
    data = Title.query.get(titleId)
    id = request.form.get('id')
    gloss = request.form.get('gloss')
    word = request.form.get('word')
    text = request.form.get('text')
    # created_at = request.form.get('created_at')
    # updated_at = request.form.get('updated_at')
    data.id =  id
    data.gloss =  gloss
    data.word =  word
    data.text =  text
    # data.created_at =  created_at
    # data.updated_at =  updated_at
    db.session.commit()
    flash('Title updated!.', category='success')
    return redirect(url_for('titles.titles_list'))

@titles.route('/title-del/<titleId>', methods=['GET','POST'])
@login_required
def title_del(titleId):
  title = Title.query.get(titleId)
  if title: 
    if request.method == 'GET':
      title = Title.query.get(titleId)
      return render_template('titles/del.html', data=title, user=current_user)
    elif  request.method == 'POST':
      db.session.delete(title)
      db.session.commit()
      flash('Title deleted!', category='success')
  return redirect(url_for('titles.titles_list'))
