import os

def rename_to_lowercase(directory):
    # Memeriksa apakah direktori ada
    if not os.path.exists(directory):
        print(f"Direktori '{directory}' tidak ada.")
        return

    # Melakukan iterasi pada direktori
    for root, dirs, files in os.walk(directory, topdown=False):
        # Mengubah nama file ke huruf kecil
        for file_name in files:
            original_file_path = os.path.join(root, file_name)
            new_file_path = os.path.join(root, file_name.lower())
            os.rename(original_file_path, new_file_path)
        
        # Mengubah nama direktori ke huruf kecil
        for dir_name in dirs:
            original_dir_path = os.path.join(root, dir_name)
            new_dir_path = os.path.join(root, dir_name.lower())
            os.rename(original_dir_path, new_dir_path)
    
    print("Semua nama folder dan file telah diubah menjadi huruf kecil.")

# Contoh penggunaan
directory_path = 'app/datasets/videos'
rename_to_lowercase(directory_path)
